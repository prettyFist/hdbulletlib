### Notes
---
This must be loaded after Hideous Destructor and before all addons.

### Contents
---
This library is limited only to the following:
- Bullets
- Loose Ammo (+ Boxes)

Magazines and spawning are out of this library's scope. On its own, the library only provides a framework. However, due to how HD generates items for ammo boxes and backpacks, options have been provided to remove those items. By default, no ammo from this mod will show up in backpacks and ammo boxes. It's better to opt-in than it is to opt-out in this case. Check out the menu.
